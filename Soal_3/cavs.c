#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <sys/stat.h>

void downloadDriveNBA() {
    pid_t child_pid = fork();
    
    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }
    
    if (child_pid == 0) {
        
        char* const wgetCmd[] = {
            "wget", "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "players.zip", 
            NULL
        };
        
        execvp("wget", wgetCmd);
        
        perror("Execvp failed");
        exit(1);
    } else {
        
        int status;
        waitpid(child_pid, &status, 0);
        
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Download berhasil.\n");
        } else {
            printf("Download gagal.\n");
        }
    }
}

void extractDriveNBA() {
    pid_t child_pid = fork();
    
    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }
    
    if (child_pid == 0) {
        char* const unzipCmd[] = {
            "unzip", "players.zip", "-d", "players", 
            NULL
        };
        
        execvp("unzip", unzipCmd);
        
        perror("Execvp failed");
        exit(1);
    } else {
        int status;
        waitpid(child_pid, &status, 0);
        
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Ekstraksi berhasil.\n");
        } else {
            printf("Ekstraksi gagal.\n");
        }
    }
}

void deleteZipDriveNBA() {
    pid_t child_pid = fork();
    
    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }
    
    if (child_pid == 0) {
        char* const rmCmd[] = {
            "rm", "players.zip", 
            NULL
        };
        
        execvp("rm", rmCmd);
        perror("Execvp failed");
        exit(1);
    } else {
        int status;
        waitpid(child_pid, &status, 0);
        
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("File ZIP berhasil dihapus.\n");
        } else {
            printf("File ZIP gagal dihapus.\n");
        }
    }
}

int isCavsPlayer(const char* filename) {
    return strstr(filename, "Cavaliers") != NULL;
}

void categorizePlayersCavs() {
    char* positions[] = {"PG", "SG", "SF", "PF", "C"};
    int playerCounts[5] = {0};

    FILE* output = fopen("Formasi.txt", "w");
    if (output == NULL) {
        perror("Failed to open Formasi.txt for categorize");
        exit(1);
    }

    DIR* dir;
    struct dirent* entry;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    // Fungsi untuk menghapus pemain yang bukan dari Cleveland Cavaliers
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            if (!isCavsPlayer(entry->d_name)) {
                char filePath[512]; 
                snprintf(filePath, sizeof(filePath), "players/%s", entry->d_name);
                // Menghapus file yang tidak termasuk dalam pemain Cleveland Cavaliers
                remove(filePath);  
            }
        }
    }
    
    closedir(dir);

    // Inisialisasi hitungan pemain untuk setiap posisi
    int Count_PG = 0, Count_SG = 0, Count_SF = 0, Count_PF = 0, Count_C = 0;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    // Fungsi untuk menghitung pemain Cavaliers berdasarkan posisi "PG", "SG", "SF", "PF", "C"
    while ((entry = readdir(dir)) != NULL) {
        if (strstr(entry->d_name, "Cavaliers") != NULL && strstr(entry->d_name, ".png") != NULL) {
            char filepath[512];
            char PosisiPlayers[10];
            snprintf(filepath, sizeof(filepath), "players/%s", entry->d_name);
            sscanf(entry->d_name, "Cavaliers-%2s", PosisiPlayers);
            char Categorize_folder[32];
            snprintf(Categorize_folder, sizeof(Categorize_folder), "players/%s", PosisiPlayers);

            struct stat st = {0};
            if (stat(Categorize_folder, &st) == -1) {
                mkdir(Categorize_folder, 0777);
            }

            char New_Folders[512];
            snprintf(New_Folders, sizeof(New_Folders), "%s/%s", Categorize_folder, entry->d_name);
            rename(filepath, New_Folders);

            if (strcmp(PosisiPlayers, "PG") == 0) Count_PG++;
            else if (strcmp(PosisiPlayers, "SG") == 0) Count_SG++;
            else if (strcmp(PosisiPlayers, "SF") == 0) Count_SF++;
            else if (strcmp(PosisiPlayers, "PF") == 0) Count_PF++;
            else if (strcmp(PosisiPlayers, "C-") == 0) Count_C++;
        }
    }
    closedir(dir);
    // Menulis informasi hitungan players ke dalam Formasi.txt
    fprintf(output, "PG: %d\n", Count_PG);
    fprintf(output, "SG: %d\n", Count_SG);
    fprintf(output, "SF: %d\n", Count_SF);
    fprintf(output, "PF: %d\n", Count_PF);
    fprintf(output, "C: %d\n", Count_C);

    fclose(output);
}

//Fungsi untuk mengkategorikan players sesuai dengan posisi masing"
void createCategoryFolderPlayersCavs() {
    
    mkdir("players/PG", 0755);
    mkdir("players/SG", 0755);
    mkdir("players/SF", 0755);
    mkdir("players/PF", 0755);
    mkdir("players/C-", 0755);
}

void categorizePngFilePlayers() {
    char* positions[] = {"PG", "SG", "SF", "PF", "C"};
    DIR* dir;
    struct dirent* entry;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char filePath[512];
            snprintf(filePath, sizeof(filePath), "players/%s", entry->d_name);

            if (isCavsPlayer(entry->d_name)) {
                for (int i = 0; i < 5; i++) {
                    if (strstr(entry->d_name, positions[i]) != NULL && strstr(entry->d_name, ".") != NULL) {
                        char destPath[512];
                        snprintf(destPath, sizeof(destPath), "players/%s/%s", positions[i], entry->d_name);
                        rename(filePath, destPath);
                    }
                }
            }
        }
    }

    closedir(dir);
}

void movePlayersCavsToClutchFolder() {
    DIR* dir;
    struct dirent* entry;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char filePath[512];
            snprintf(filePath, sizeof(filePath), "players/%s", entry->d_name);

            if (isCavsPlayer(entry->d_name)) {
                if (strstr(entry->d_name, "LeBron-James") != NULL || strstr(entry->d_name, "Kyrie-Irving") != NULL) {
                    char destPath[512];
                    snprintf(destPath, sizeof(destPath), "clutch/%s", entry->d_name);
                    rename(filePath, destPath);
                }
            }
        }
    }

    closedir(dir);
}

void createClutchFolder() {
    mkdir("clutch", 0755);
}


int main() {
    pid_t child_pid;

    child_pid = fork();

    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }

    if (child_pid == 0) {
        downloadDriveNBA();
        extractDriveNBA();
        deleteZipDriveNBA();
        categorizePlayersCavs();
        createCategoryFolderPlayersCavs();
        createClutchFolder();
        movePlayersCavsToClutchFolder();
        sleep(2);
        
        // Fungsi mengkategorikan file .png dan dimasukkan ke dalam folder kategori yang sesuai dengan posisinya
       categorizePngFilePlayers();
        
        printf("Selesai\n");
    } else {
        wait(NULL);
    }

    return 0;
}