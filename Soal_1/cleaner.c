#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>



int should_sleep = 1;

void write_log(const char *file_path) {
    time_t raw_time;
    struct tm *time_info;
    char time_str[20];
    time(&raw_time);
    time_info = localtime(&raw_time);
    strftime(time_str, sizeof(time_str), "%Y-%m-%d %H:%M:%S", time_info);

    char log_entry[256];
    snprintf(log_entry, sizeof(log_entry), "[%s] '%s' has been removed.\n", time_str, file_path);

    openlog("cleaner", LOG_PID | LOG_NDELAY | LOG_NOWAIT | LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "%s", log_entry);
    closelog();
}



void search_directory(const char *dir_path) {
    DIR *dir;
    struct dirent *entry;
    char path[1024];

    if ((dir = opendir(dir_path)) == NULL) {
        openlog("cleaner", LOG_PID | LOG_NDELAY | LOG_NOWAIT | LOG_CONS, LOG_USER);
        syslog(LOG_ERR, "Could not open directory: %s", dir_path);
        closelog();
        return;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }
            snprintf(path, sizeof(path), "%s/%s", dir_path, entry->d_name);
            search_directory(path);
        } else {
            if (strstr(entry->d_name, ".txt") != NULL && strstr(entry->d_name, "SUSPICIOUS") != NULL) {
                snprintf(path, sizeof(path), "%s/%s", dir_path, entry->d_name);
                if (remove(path) == 0) {
                    write_log(path);
                    should_sleep = 0; // atur should_sleep menjadi 0 biar ga tidur setelah ini
                } else {
                    openlog("cleaner", LOG_PID | LOG_NDELAY | LOG_NOWAIT | LOG_CONS, LOG_USER);
                    syslog(LOG_ERR, "Failed to remove file: %s", path);
                    closelog();
                }
                return; // Keluar dari fungsi setelah menghapus file
            }
        }
    }

    closedir(dir);
}




int main(int argc, char **argv) {
    if (argc != 2) {
        printf("Usage: %s '/path/to/your/directory'\n", argv[0]);
        return 1;
    }

    pid_t pid;

    // hentikan proses utama
    pid = fork();
    if (pid < 0) {
        perror("Fork failed");
        exit(EXIT_FAILURE);
    }

    // keluar PID jika mendapatkan proses induk
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    // ubah file
    umask(0);

    // SID baru untuk proses
    if (setsid() < 0) {
        perror("setsid failed");
        exit(EXIT_FAILURE);
    }

    // ubah direktori jadi root
    if (chdir("/") < 0) {
        perror("chdir failed");
        exit(EXIT_FAILURE);
    }

    // tutup deskriptor file standar
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        // Loop satu file satu waktu
        should_sleep = 1; // atur should_sleep jadi 1 sebelum mencari file
        search_directory("/home/dhanixyi/inidir");

        if (should_sleep) {
            // Jika should_sleep masih 1, tidur selama 30 detik
            sleep(30);
        }
    }

    return 0;
}

